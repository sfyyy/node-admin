const config = {
  mysqlConfig: {
    user: "root",
    password: "1234qwer",
    database: "node-admin",
    host: "localhost",
    port: 3306,
    timezone: "08:00",
  },
  TOKEN: {
    PRIVITE_KEY: "18d1ceba8d794077acc89adc10e904b8",
    EXPIRESD: 60 * 60 * 24,
  },
  REDIS: {
    port: 6379,
    host: "127.0.0.1",
  },
};

// module.exports = mysqlConfig;
module.exports = config;
