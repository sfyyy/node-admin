//配置连接池
const mysql = require("mysql2");
const config = require("../config/config.js");

var pool = mysql.createPool(config.mysqlConfig);

/*连接数据库*/

let query = (sql, value) => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        reject(err);
      } else {
        connection.query(sql, value, (err, row) => {
          if (err) reject(err);
          else {
            resolve(row);
          }
          console.log("连接数据库成功");
          connection.release();
        });
      }
    });
  });
};

module.exports = {
  query,
};
