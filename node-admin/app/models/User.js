const { sequelize } = require("../../lib/db");
const { Sequelize, DataTypes, UUIDV4 } = require("sequelize");

const Users = sequelize.define("User", {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    default: "",
  },
  id: {
    type: DataTypes.UUID,
    default: UUIDV4,
    primaryKey: true,
  },
});

module.exports = Users;
