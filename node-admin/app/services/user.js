const Users = require("../models/User");

class UserService {
  //测试使用
  static async ceshi() {
    return await Users.create({ name: "测试" }).then((res, err) => {
      console.log(res, "res");
      if (err) {
        throw err;
      }
      return { data: null, message: "新增成功" };
    });
  }
}

module.exports = UserService;
