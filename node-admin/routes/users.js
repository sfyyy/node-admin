const router = require("koa-router")();
const User = require("../app/services/user");
router.prefix("/users");

router.get("/", async (ctx, next) => {
  const result = await User.ceshi();
  ctx.success(result.data, result.message);

  // ctx.body = 'this is a users response!'
});

router.get("/bar", function (ctx, next) {
  ctx.body = "this is a users/bar response";
});

module.exports = router;
