module.exports = () => {
  return async (ctx, next) => {
    try {
      ctx.success = (data, msg) => {
        ctx.body = {
          code: 200,
          data,
          message: msg,
        };
      };
      await next();
    } catch (e) {
      ctx.body = { status: e.status, message: e.message };
    }
  };
};
